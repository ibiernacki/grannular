﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Grannular.Lib;
using MathNet.Numerics.LinearAlgebra.Double;
using MathNet.Numerics.LinearAlgebra.Storage;
using Newtonsoft.Json;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace Grannular.Tests
{
    [TestFixture]
    public class Tests
    {
        [TestCase("Data\\housing.dat", 1e-6, 2d, 8)]
        [TestCase("Data\\testdata.dat", 1e-6, 2d, 5)]
        [TestCase("Data\\winequality-white.dat", 1e-6, 1.2, 7)]
        public void CodeTest(string datafile, double epsilon, double fuzziness, int clusters)
        {
            var dr = new DataReader();
            var cfg = dr.Read(new FileInfo(datafile));

            cfg.ClustersCount = clusters;
            cfg.Epsilon = epsilon;
            cfg.Fuzziness = fuzziness;


            var fcm = new FCMTS(cfg);
            fcm.RunFCM();

            var _y = cfg.Data.Column(1);
            var msr = fcm.RunTS(_y);
        }

        [TestCase("Data\\winequality-white.dat", "Data\\WineQuality-white.json")]
        [TestCase("Data\\winequality-red.dat", "Data\\WineQuality-red.json")]
        [TestCase("Data\\cpu-performance.dat", "Data\\cpu-performance.json")]
        [TestCase("Data\\abalone.dat", "Data\\abalone.json")]
        [TestCase("Data\\auto-mpg.dat", "Data\\auto-mpg.json")]
        [TestCase("Data\\glass.dat", "Data\\glass.json")]
        [TestCase("Data\\housing.dat", "Data\\housing.json")]
        [TestCase("Data\\page-blocks.dat", "Data\\page-blocks.json")]
        [TestCase("Data\\japanese-vowels.dat", "Data\\japanese-vowels.json")]
        [TestCase("Data\\pima-indians-diabetes.dat", "Data\\pima-indians-diabetes.json")]
        public void SplitData(string dataFile, string outputFile)
        {
            var dr = new DataReader();
            var cfg = dr.Read(new FileInfo(dataFile));

            DenseMatrix teachDataMatrix;
            DenseMatrix testDataMatrix;

            DataSplitter.Split(cfg.Data, out teachDataMatrix, out testDataMatrix);

            var teachData = teachDataMatrix.Storage.ToArray();
            var testData = testDataMatrix.Storage.ToArray();

            var teachDataJson = JsonConvert.SerializeObject(teachData, Formatting.Indented);
            var testDataJson = JsonConvert.SerializeObject(testData, Formatting.Indented);

            File.WriteAllText(
                outputFile.Replace(Path.GetFileNameWithoutExtension(outputFile),
                    Path.GetFileNameWithoutExtension(outputFile) + "-teach"), teachDataJson);
            File.WriteAllText(
                outputFile.Replace(Path.GetFileNameWithoutExtension(outputFile),
                    Path.GetFileNameWithoutExtension(outputFile) + "-test"), testDataJson);
        }

        #region Wine quality white

        [TestCase("Data\\winequality-white-teach.json", "Results\\WineQuality-white-teach.json", 5, 1.5,
            Category = "Wine Quality (white)")]
        [TestCase("Data\\winequality-white-teach.json", "Results\\WineQuality-white-teach.json", 7, 1.5,
            Category = "Wine Quality (white)")]
        [TestCase("Data\\winequality-white-teach.json", "Results\\WineQuality-white-teach.json", 11, 1.5,
            Category = "Wine Quality (white)")]
        [TestCase("Data\\winequality-white-teach.json", "Results\\WineQuality-white-teach.json", 7, 1.8,
            Category = "Wine Quality (white)")]
        [TestCase("Data\\winequality-white-teach.json", "Results\\WineQuality-white-teach.json", 7, 1.3,
            Category = "Wine Quality (white)")]
        [TestCase("Data\\winequality-white-test.json", "Results\\WineQuality-white-test.json", 5, 1.5,
            Category = "Wine Quality (white)")]
        [TestCase("Data\\winequality-white-test.json", "Results\\WineQuality-white-test.json", 7, 1.5,
            Category = "Wine Quality (white)")]
        [TestCase("Data\\winequality-white-test.json", "Results\\WineQuality-white-test.json", 11, 1.5,
            Category = "Wine Quality (white)")]
        [TestCase("Data\\winequality-white-test.json", "Results\\WineQuality-white-test.json", 7, 1.8,
            Category = "Wine Quality (white)")]
        [TestCase("Data\\winequality-white-test.json", "Results\\WineQuality-white-test.json", 7, 1.3,
            Category = "Wine Quality (white)")]

        #endregion

        #region Wine quality red

        [TestCase("Data\\winequality-red-teach.json", "Results\\WineQuality-red-teach.json", 5, 1.5,
            Category = "Wine Quality (red)")]
        [TestCase("Data\\winequality-red-teach.json", "Results\\WineQuality-red-teach.json", 7, 1.5,
            Category = "Wine Quality (red)")]
        [TestCase("Data\\winequality-red-teach.json", "Results\\WineQuality-red-teach.json", 11, 1.5,
            Category = "Wine Quality (red)")]
        [TestCase("Data\\winequality-red-teach.json", "Results\\WineQuality-red-teach.json", 7, 1.8,
            Category = "Wine Quality (red)")]
        [TestCase("Data\\winequality-red-teach.json", "Results\\WineQuality-red-teach.json", 7, 1.3,
            Category = "Wine Quality (red)")]
        [TestCase("Data\\winequality-red-test.json", "Results\\WineQuality-red-test.json", 5, 1.5,
            Category = "Wine Quality (red)")]
        [TestCase("Data\\winequality-red-test.json", "Results\\WineQuality-red-test.json", 7, 1.5,
            Category = "Wine Quality (red)")]
        [TestCase("Data\\winequality-red-test.json", "Results\\WineQuality-red-test.json", 11, 1.5,
            Category = "Wine Quality (red)")]
        [TestCase("Data\\winequality-red-test.json", "Results\\WineQuality-red-test.json", 7, 1.8,
            Category = "Wine Quality (red)")]
        [TestCase("Data\\winequality-red-test.json", "Results\\WineQuality-red-test.json", 7, 1.3,
            Category = "Wine Quality (red)")]

        #endregion

        #region CPU performance

        [TestCase("Data\\cpu-performance-teach.json", "Results\\cpu-performance-teach.json", 2, 1.4,
            Category = "CPU Performance")]
        [TestCase("Data\\cpu-performance-teach.json", "Results\\cpu-performance-teach.json", 2, 2,
            Category = "CPU Performance")]
        [TestCase("Data\\cpu-performance-teach.json", "Results\\cpu-performance-teach.json", 3, 1.4,
            Category = "CPU Performance")]
        [TestCase("Data\\cpu-performance-teach.json", "Results\\cpu-performance-teach.json", 3, 2,
            Category = "CPU Performance")]
        [TestCase("Data\\cpu-performance-teach.json", "Results\\cpu-performance-teach.json", 3, 2.5,
            Category = "CPU Performance")]
        [TestCase("Data\\cpu-performance-teach.json", "Results\\cpu-performance-teach.json", 6, 1.5,
            Category = "CPU Performance")]
        [TestCase("Data\\cpu-performance-teach.json", "Results\\cpu-performance-teach.json", 6, 2,
            Category = "CPU Performance")]
        [TestCase("Data\\cpu-performance-teach.json", "Results\\cpu-performance-teach.json", 7, 1.5,
            Category = "CPU Performance")]
        [TestCase("Data\\cpu-performance-teach.json", "Results\\cpu-performance-teach.json", 7, 2,
            Category = "CPU Performance")]

        [TestCase("Data\\cpu-performance-test.json", "Results\\cpu-performance-test.json", 2, 1.4,
            Category = "CPU Performance")]
        [TestCase("Data\\cpu-performance-test.json", "Results\\cpu-performance-test.json", 2, 2,
            Category = "CPU Performance")]
        [TestCase("Data\\cpu-performance-test.json", "Results\\cpu-performance-test.json", 3, 1.4,
            Category = "CPU Performance")]
        [TestCase("Data\\cpu-performance-test.json", "Results\\cpu-performance-test.json", 3, 2,
            Category = "CPU Performance")]
        [TestCase("Data\\cpu-performance-test.json", "Results\\cpu-performance-test.json", 3, 2.5,
            Category = "CPU Performance")]
        [TestCase("Data\\cpu-performance-test.json", "Results\\cpu-performance-test.json", 6, 1.5,
            Category = "CPU Performance")]
        [TestCase("Data\\cpu-performance-test.json", "Results\\cpu-performance-test.json", 6, 2,
            Category = "CPU Performance")]
        [TestCase("Data\\cpu-performance-test.json", "Results\\cpu-performance-test.json", 7, 1.5,
            Category = "CPU Performance")]
        [TestCase("Data\\cpu-performance-test.json", "Results\\cpu-performance-test.json", 7, 2,
            Category = "CPU Performance")]

        #endregion

        #region Abalone

        [TestCase("Data\\abalone-teach.json", "Results\\abalone-teach.json", 6, 1.2, Category = "Abalone")]
        [TestCase("Data\\abalone-test.json", "Results\\abalone-test.json", 6, 1.2, Category = "Abalone")]
        [TestCase("Data\\abalone-teach.json", "Results\\abalone-teach.json", 10, 1.2, Category = "Abalone")]
        [TestCase("Data\\abalone-test.json", "Results\\abalone-test.json", 10, 1.2, Category = "Abalone")]
        [TestCase("Data\\abalone-teach.json", "Results\\abalone-teach.json", 10, 1.4, Category = "Abalone")]
        [TestCase("Data\\abalone-test.json", "Results\\abalone-test.json", 10, 1.4, Category = "Abalone")]
        [TestCase("Data\\abalone-teach.json", "Results\\abalone-teach.json", 10, 1.6, Category = "Abalone")]
        [TestCase("Data\\abalone-test.json", "Results\\abalone-test.json", 10, 1.6, Category = "Abalone")]
        [TestCase("Data\\abalone-teach.json", "Results\\abalone-teach.json", 12, 1.2, Category = "Abalone")]
        [TestCase("Data\\abalone-test.json", "Results\\abalone-test.json", 12, 1.2, Category = "Abalone")]
        [TestCase("Data\\abalone-teach.json", "Results\\abalone-teach.json", 8, 1.2, Category = "Abalone")]
        [TestCase("Data\\abalone-test.json", "Results\\abalone-test.json", 8, 1.2, Category = "Abalone")]
        [TestCase("Data\\abalone-teach.json", "Results\\abalone-teach.json", 8, 1.6, Category = "Abalone")]
        [TestCase("Data\\abalone-test.json", "Results\\abalone-test.json", 8, 1.6, Category = "Abalone")]
        [TestCase("Data\\abalone-teach.json", "Results\\abalone-teach.json", 14, 1.2, Category = "Abalone")]
        [TestCase("Data\\abalone-test.json", "Results\\abalone-test.json", 14, 1.2, Category = "Abalone")]
        [TestCase("Data\\abalone-teach.json", "Results\\abalone-teach.json", 14, 1.6, Category = "Abalone")]
        [TestCase("Data\\abalone-test.json", "Results\\abalone-test.json", 14, 1.6, Category = "Abalone")]

        #endregion

        #region Auto mpg

        [TestCase("Data\\auto-mpg-teach.json", "Results\\auto-mpg-teach.json", 5, 1.2, Category = "Auto MPG")]
        [TestCase("Data\\auto-mpg-test.json", "Results\\auto-mpg-test.json", 5, 1.2, Category = "Auto MPG")]
        [TestCase("Data\\auto-mpg-teach.json", "Results\\auto-mpg-teach.json", 5, 1.4, Category = "Auto MPG")]
        [TestCase("Data\\auto-mpg-test.json", "Results\\auto-mpg-test.json", 5, 1.4, Category = "Auto MPG")]
        [TestCase("Data\\auto-mpg-teach.json", "Results\\auto-mpg-teach.json", 5, 1.8, Category = "Auto MPG")]
        [TestCase("Data\\auto-mpg-test.json", "Results\\auto-mpg-test.json", 5, 1.8, Category = "Auto MPG")]
        [TestCase("Data\\auto-mpg-teach.json", "Results\\auto-mpg-teach.json", 5, 2.4, Category = "Auto MPG")]
        [TestCase("Data\\auto-mpg-test.json", "Results\\auto-mpg-test.json", 5, 2.4, Category = "Auto MPG")]
        [TestCase("Data\\auto-mpg-teach.json", "Results\\auto-mpg-teach.json", 8, 1.2, Category = "Auto MPG")]
        [TestCase("Data\\auto-mpg-test.json", "Results\\auto-mpg-test.json", 8, 1.2, Category = "Auto MPG")]
        [TestCase("Data\\auto-mpg-teach.json", "Results\\auto-mpg-teach.json", 8, 1.4, Category = "Auto MPG")]
        [TestCase("Data\\auto-mpg-test.json", "Results\\auto-mpg-test.json", 8, 1.4, Category = "Auto MPG")]
        [TestCase("Data\\auto-mpg-teach.json", "Results\\auto-mpg-teach.json", 8, 1.8, Category = "Auto MPG")]
        [TestCase("Data\\auto-mpg-test.json", "Results\\auto-mpg-test.json", 8, 1.8, Category = "Auto MPG")]
        [TestCase("Data\\auto-mpg-teach.json", "Results\\auto-mpg-teach.json", 8, 2.4, Category = "Auto MPG")]
        [TestCase("Data\\auto-mpg-test.json", "Results\\auto-mpg-test.json", 8, 2.4, Category = "Auto MPG")]
        [TestCase("Data\\auto-mpg-teach.json", "Results\\auto-mpg-teach.json", 12, 1.2, Category = "Auto MPG")]
        [TestCase("Data\\auto-mpg-test.json", "Results\\auto-mpg-test.json", 12, 1.2, Category = "Auto MPG")]
        [TestCase("Data\\auto-mpg-teach.json", "Results\\auto-mpg-teach.json", 12, 2.0, Category = "Auto MPG")]
        [TestCase("Data\\auto-mpg-test.json", "Results\\auto-mpg-test.json", 12, 2.0, Category = "Auto MPG")]

        #endregion

        #region Housing

        [TestCase("Data\\housing-teach.json", "Results\\housing-teach.json", 6, 1.2, Category = "Housing")]
        [TestCase("Data\\housing-test.json", "Results\\housing-test.json", 6, 1.2, Category = "Housing")]
        [TestCase("Data\\housing-teach.json", "Results\\housing-teach.json", 6, 1.4, Category = "Housing")]
        [TestCase("Data\\housing-test.json", "Results\\housing-test.json", 6, 1.4, Category = "Housing")]
        [TestCase("Data\\housing-teach.json", "Results\\housing-teach.json", 6, 1.8, Category = "Housing")]
        [TestCase("Data\\housing-test.json", "Results\\housing-test.json", 6, 1.8, Category = "Housing")]
        [TestCase("Data\\housing-teach.json", "Results\\housing-teach.json", 6, 2.4, Category = "Housing")]
        [TestCase("Data\\housing-test.json", "Results\\housing-test.json", 6, 2.4, Category = "Housing")]


        [TestCase("Data\\housing-teach.json", "Results\\housing-teach.json", 10, 1.2, Category = "Housing")]
        [TestCase("Data\\housing-test.json", "Results\\housing-test.json", 10, 1.2, Category = "Housing")]
        [TestCase("Data\\housing-teach.json", "Results\\housing-teach.json", 10, 1.6, Category = "Housing")]
        [TestCase("Data\\housing-test.json", "Results\\housing-test.json", 10, 1.6, Category = "Housing")]
        [TestCase("Data\\housing-teach.json", "Results\\housing-teach.json", 10, 2.0, Category = "Housing")]
        [TestCase("Data\\housing-test.json", "Results\\housing-test.json", 10, 2.0, Category = "Housing")]

        [TestCase("Data\\housing-teach.json", "Results\\housing-teach.json", 12, 1.2, Category = "Housing")]
        [TestCase("Data\\housing-test.json", "Results\\housing-test.json", 12, 1.2, Category = "Housing")]
        [TestCase("Data\\housing-teach.json", "Results\\housing-teach.json", 12, 1.6, Category = "Housing")]
        [TestCase("Data\\housing-test.json", "Results\\housing-test.json", 12, 1.6, Category = "Housing")]
        [TestCase("Data\\housing-teach.json", "Results\\housing-teach.json", 12, 2.0, Category = "Housing")]
        [TestCase("Data\\housing-test.json", "Results\\housing-test.json", 12, 2.0, Category = "Housing")]

        [TestCase("Data\\housing-teach.json", "Results\\housing-teach.json", 16, 1.2, Category = "Housing")]
        [TestCase("Data\\housing-test.json", "Results\\housing-test.json", 16, 1.2, Category = "Housing")]
        [TestCase("Data\\housing-teach.json", "Results\\housing-teach.json", 16, 1.6, Category = "Housing")]
        [TestCase("Data\\housing-test.json", "Results\\housing-test.json", 16, 1.6, Category = "Housing")]
        [TestCase("Data\\housing-teach.json", "Results\\housing-teach.json", 16, 2.0, Category = "Housing")]
        [TestCase("Data\\housing-test.json", "Results\\housing-test.json", 16, 2.0, Category = "Housing")]

        [TestCase("Data\\housing-teach.json", "Results\\housing-teach.json", 20, 1.2, Category = "Housing")]
        [TestCase("Data\\housing-test.json", "Results\\housing-test.json", 20, 1.2, Category = "Housing")]
        [TestCase("Data\\housing-teach.json", "Results\\housing-teach.json", 20, 1.6, Category = "Housing")]
        [TestCase("Data\\housing-test.json", "Results\\housing-test.json", 20, 1.6, Category = "Housing")]
        [TestCase("Data\\housing-teach.json", "Results\\housing-teach.json", 20, 2.0, Category = "Housing")]
        [TestCase("Data\\housing-test.json", "Results\\housing-test.json", 20, 2.0, Category = "Housing")]

        #endregion

        #region Page Blocks (11 columns)
        [TestCase("Data\\page-blocks-teach.json", "Results\\page-blocks-teach.json", 4, 1.1, Category = "Page Blocks")]
        [TestCase("Data\\page-blocks-test.json", "Results\\page-blocks-test.json", 4, 1.1, Category = "Page Blocks")]
        [TestCase("Data\\page-blocks-teach.json", "Results\\page-blocks-teach.json", 4, 1.2, Category = "Page Blocks")]
        [TestCase("Data\\page-blocks-test.json", "Results\\page-blocks-test.json", 4, 1.2, Category = "Page Blocks")]
        [TestCase("Data\\page-blocks-teach.json", "Results\\page-blocks-teach.json", 4, 1.4, Category = "Page Blocks")]
        [TestCase("Data\\page-blocks-test.json", "Results\\page-blocks-test.json", 4, 1.4, Category = "Page Blocks")]
        [TestCase("Data\\page-blocks-teach.json", "Results\\page-blocks-teach.json", 4, 1.6, Category = "Page Blocks")]
        [TestCase("Data\\page-blocks-test.json", "Results\\page-blocks-test.json", 4, 1.6, Category = "Page Blocks")]
        [TestCase("Data\\page-blocks-teach.json", "Results\\page-blocks-teach.json", 6, 1.2, Category = "Page Blocks")]
        [TestCase("Data\\page-blocks-test.json", "Results\\page-blocks-test.json", 6, 1.2, Category = "Page Blocks")]
        [TestCase("Data\\page-blocks-teach.json", "Results\\page-blocks-teach.json", 6, 1.4, Category = "Page Blocks")]
        [TestCase("Data\\page-blocks-test.json", "Results\\page-blocks-test.json", 6, 1.4, Category = "Page Blocks")]
        [TestCase("Data\\page-blocks-teach.json", "Results\\page-blocks-teach.json", 6, 1.6, Category = "Page Blocks")]
        [TestCase("Data\\page-blocks-test.json", "Results\\page-blocks-test.json", 6, 1.6, Category = "Page Blocks")]
        [TestCase("Data\\page-blocks-teach.json", "Results\\page-blocks-teach.json", 8, 1.2, Category = "Page Blocks")]
        [TestCase("Data\\page-blocks-test.json", "Results\\page-blocks-test.json", 8, 1.2, Category = "Page Blocks")]
        [TestCase("Data\\page-blocks-teach.json", "Results\\page-blocks-teach.json", 8, 1.4, Category = "Page Blocks")]
        [TestCase("Data\\page-blocks-test.json", "Results\\page-blocks-test.json", 8, 1.4, Category = "Page Blocks")]
        [TestCase("Data\\page-blocks-teach.json", "Results\\page-blocks-teach.json", 8, 1.6, Category = "Page Blocks")]
        [TestCase("Data\\page-blocks-test.json", "Results\\page-blocks-test.json", 8, 1.6, Category = "Page Blocks")]
        [TestCase("Data\\page-blocks-teach.json", "Results\\page-blocks-teach.json", 10, 1.2, Category = "Page Blocks")]
        [TestCase("Data\\page-blocks-test.json", "Results\\page-blocks-test.json", 10, 1.2, Category = "Page Blocks")]
        [TestCase("Data\\page-blocks-teach.json", "Results\\page-blocks-teach.json", 10, 1.4, Category = "Page Blocks")]
        [TestCase("Data\\page-blocks-test.json", "Results\\page-blocks-test.json", 10, 1.4, Category = "Page Blocks")]
        [TestCase("Data\\page-blocks-teach.json", "Results\\page-blocks-teach.json", 10, 1.6, Category = "Page Blocks")]
        [TestCase("Data\\page-blocks-test.json", "Results\\page-blocks-test.json", 10, 1.6, Category = "Page Blocks")]

        #endregion

        #region Japanese Vowels (12 columns)
        [TestCase("Data\\japanese-vowels-teach.json", "Results\\japanese-vowels-teach.json", 10, 1.2,
            Category = "Japanese Vowels")]
        [TestCase("Data\\japanese-vowels-test.json", "Results\\japanese-vowels-test.json", 10, 1.2,
            Category = "Japanese Vowels")]
        [TestCase("Data\\japanese-vowels-teach.json", "Results\\japanese-vowels-teach.json", 10, 1.8,
            Category = "Japanese Vowels")]
        [TestCase("Data\\japanese-vowels-test.json", "Results\\japanese-vowels-test.json", 10, 1.8,
            Category = "Japanese Vowels")]
        [TestCase("Data\\japanese-vowels-teach.json", "Results\\japanese-vowels-teach.json", 10, 2.4,
            Category = "Japanese Vowels")]
        [TestCase("Data\\japanese-vowels-test.json", "Results\\japanese-vowels-test.json", 10, 2.4,
            Category = "Japanese Vowels")]

        [TestCase("Data\\japanese-vowels-teach.json", "Results\\japanese-vowels-teach.json", 14, 1.8,
            Category = "Japanese Vowels")]
        [TestCase("Data\\japanese-vowels-test.json", "Results\\japanese-vowels-test.json", 14, 1.8,
            Category = "Japanese Vowels")]
        [TestCase("Data\\japanese-vowels-teach.json", "Results\\japanese-vowels-teach.json", 14, 2.4,
            Category = "Japanese Vowels")]
        [TestCase("Data\\japanese-vowels-test.json", "Results\\japanese-vowels-test.json", 14, 2.4,
            Category = "Japanese Vowels")]

        [TestCase("Data\\japanese-vowels-teach.json", "Results\\japanese-vowels-teach.json", 5, 1.2,
            Category = "Japanese Vowels")]
        [TestCase("Data\\japanese-vowels-test.json", "Results\\japanese-vowels-test.json", 5, 1.2,
            Category = "Japanese Vowels")]
        [TestCase("Data\\japanese-vowels-teach.json", "Results\\japanese-vowels-teach.json", 5, 1.5,
            Category = "Japanese Vowels")]
        [TestCase("Data\\japanese-vowels-test.json", "Results\\japanese-vowels-test.json", 5, 1.5,
            Category = "Japanese Vowels")]

        [TestCase("Data\\japanese-vowels-teach.json", "Results\\japanese-vowels-teach.json", 7, 1.2,
            Category = "Japanese Vowels")]
        [TestCase("Data\\japanese-vowels-test.json", "Results\\japanese-vowels-test.json", 7, 1.2,
            Category = "Japanese Vowels")]
        [TestCase("Data\\japanese-vowels-teach.json", "Results\\japanese-vowels-teach.json", 7, 1.5,
            Category = "Japanese Vowels")]
        [TestCase("Data\\japanese-vowels-test.json", "Results\\japanese-vowels-test.json", 7, 1.5,
            Category = "Japanese Vowels")]
        [TestCase("Data\\japanese-vowels-teach.json", "Results\\japanese-vowels-teach.json", 7, 1.7,
            Category = "Japanese Vowels")]
        [TestCase("Data\\japanese-vowels-test.json", "Results\\japanese-vowels-test.json", 7, 1.7,
            Category = "Japanese Vowels")]

        #endregion

        #region Glass (10 columns)
        [TestCase("Data\\glass-teach.json", "Results\\glass-teach.json", 3, 1.1, Category = "Glass")]
        [TestCase("Data\\glass-test.json", "Results\\glass-test.json", 3, 1.1, Category = "Glass")]
        [TestCase("Data\\glass-teach.json", "Results\\glass-teach.json", 3, 1.3, Category = "Glass")]
        [TestCase("Data\\glass-test.json", "Results\\glass-test.json", 3, 1.3, Category = "Glass")]
        [TestCase("Data\\glass-teach.json", "Results\\glass-teach.json", 3, 1.5, Category = "Glass")]
        [TestCase("Data\\glass-test.json", "Results\\glass-test.json", 3, 1.5, Category = "Glass")]

        [TestCase("Data\\glass-teach.json", "Results\\glass-teach.json", 5, 1.3, Category = "Glass")]
        [TestCase("Data\\glass-test.json", "Results\\glass-test.json", 5, 1.3, Category = "Glass")]
        [TestCase("Data\\glass-teach.json", "Results\\glass-teach.json", 5, 1.5, Category = "Glass")]
        [TestCase("Data\\glass-test.json", "Results\\glass-test.json", 5, 1.5, Category = "Glass")]
        [TestCase("Data\\glass-teach.json", "Results\\glass-teach.json", 5, 1.8, Category = "Glass")]
        [TestCase("Data\\glass-test.json", "Results\\glass-test.json", 5, 1.8, Category = "Glass")]
        [TestCase("Data\\glass-teach.json", "Results\\glass-teach.json", 5, 2.0, Category = "Glass")]
        [TestCase("Data\\glass-test.json", "Results\\glass-test.json", 5, 2.0, Category = "Glass")]
        [TestCase("Data\\glass-teach.json", "Results\\glass-teach.json", 5, 2.4, Category = "Glass")]
        [TestCase("Data\\glass-test.json", "Results\\glass-test.json", 5, 2.4, Category = "Glass")]

        [TestCase("Data\\glass-teach.json", "Results\\glass-teach.json", 7, 1.3, Category = "Glass")]
        [TestCase("Data\\glass-test.json", "Results\\glass-test.json", 7, 1.3, Category = "Glass")]
        [TestCase("Data\\glass-teach.json", "Results\\glass-teach.json", 7, 1.5, Category = "Glass")]
        [TestCase("Data\\glass-test.json", "Results\\glass-test.json", 7, 1.5, Category = "Glass")]
        [TestCase("Data\\glass-teach.json", "Results\\glass-teach.json", 7, 1.8, Category = "Glass")]
        [TestCase("Data\\glass-test.json", "Results\\glass-test.json", 7, 1.8, Category = "Glass")]
        [TestCase("Data\\glass-teach.json", "Results\\glass-teach.json", 7, 2.0, Category = "Glass")]
        [TestCase("Data\\glass-test.json", "Results\\glass-test.json", 7, 2.0, Category = "Glass")]
        [TestCase("Data\\glass-teach.json", "Results\\glass-teach.json", 7, 2.4, Category = "Glass")]
        [TestCase("Data\\glass-test.json", "Results\\glass-test.json", 7, 2.4, Category = "Glass")]

        [TestCase("Data\\glass-teach.json", "Results\\glass-teach.json", 10, 1.5, Category = "Glass")]
        [TestCase("Data\\glass-test.json", "Results\\glass-test.json", 10, 1.5, Category = "Glass")]
        [TestCase("Data\\glass-teach.json", "Results\\glass-teach.json", 10, 1.8, Category = "Glass")]
        [TestCase("Data\\glass-test.json", "Results\\glass-test.json", 10, 1.8, Category = "Glass")]
        [TestCase("Data\\glass-teach.json", "Results\\glass-teach.json", 10, 2.0, Category = "Glass")]
        [TestCase("Data\\glass-test.json", "Results\\glass-test.json", 10, 2.0, Category = "Glass")]
        [TestCase("Data\\glass-teach.json", "Results\\glass-teach.json", 10, 2.4, Category = "Glass")]
        [TestCase("Data\\glass-test.json", "Results\\glass-test.json", 10, 2.4, Category = "Glass")]


        [TestCase("Data\\glass-teach.json", "Results\\glass-teach.json", 14, 1.5, Category = "Glass")]
        [TestCase("Data\\glass-test.json", "Results\\glass-test.json", 14, 1.5, Category = "Glass")]
        [TestCase("Data\\glass-teach.json", "Results\\glass-teach.json", 14, 1.8, Category = "Glass")]
        [TestCase("Data\\glass-test.json", "Results\\glass-test.json", 14, 1.8, Category = "Glass")]
        [TestCase("Data\\glass-teach.json", "Results\\glass-teach.json", 14, 2.0, Category = "Glass")]
        [TestCase("Data\\glass-test.json", "Results\\glass-test.json", 14, 2.0, Category = "Glass")]
        [TestCase("Data\\glass-teach.json", "Results\\glass-teach.json", 14, 2.4, Category = "Glass")]
        [TestCase("Data\\glass-test.json", "Results\\glass-test.json", 14, 2.4, Category = "Glass")]
        #endregion

        #region Pima Indians Diabetes (9 columns)
        [TestCase("Data\\pima-indians-diabetes-teach.json", "Results\\pima-indians-diabetes-teach.json", 3, 1.1, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-test.json", "Results\\pima-indians-diabetes-test.json", 3, 1.1, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-teach.json", "Results\\pima-indians-diabetes-teach.json", 3, 1.3, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-test.json", "Results\\pima-indians-diabetes-test.json", 3, 1.3, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-teach.json", "Results\\pima-indians-diabetes-teach.json", 3, 1.6, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-test.json", "Results\\pima-indians-diabetes-test.json", 3, 1.6, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-teach.json", "Results\\pima-indians-diabetes-teach.json", 3, 1.8, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-test.json", "Results\\pima-indians-diabetes-test.json", 3, 1.8, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-teach.json", "Results\\pima-indians-diabetes-teach.json", 3, 2, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-test.json", "Results\\pima-indians-diabetes-test.json", 3, 2, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-teach.json", "Results\\pima-indians-diabetes-teach.json", 3, 2.4, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-test.json", "Results\\pima-indians-diabetes-test.json", 3, 2.4, Category = "Pima Indians Diabetes")]

        [TestCase("Data\\pima-indians-diabetes-teach.json", "Results\\pima-indians-diabetes-teach.json", 4, 1.1, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-test.json", "Results\\pima-indians-diabetes-test.json", 4, 1.1, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-teach.json", "Results\\pima-indians-diabetes-teach.json", 4, 1.3, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-test.json", "Results\\pima-indians-diabetes-test.json", 4, 1.3, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-teach.json", "Results\\pima-indians-diabetes-teach.json", 4, 1.6, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-test.json", "Results\\pima-indians-diabetes-test.json", 4, 1.6, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-teach.json", "Results\\pima-indians-diabetes-teach.json", 4, 1.8, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-test.json", "Results\\pima-indians-diabetes-test.json", 4, 1.8, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-teach.json", "Results\\pima-indians-diabetes-teach.json", 4, 2, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-test.json", "Results\\pima-indians-diabetes-test.json", 4, 2, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-teach.json", "Results\\pima-indians-diabetes-teach.json", 4, 2.4, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-test.json", "Results\\pima-indians-diabetes-test.json", 4, 2.4, Category = "Pima Indians Diabetes")]

        [TestCase("Data\\pima-indians-diabetes-teach.json", "Results\\pima-indians-diabetes-teach.json", 6, 1.1, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-test.json", "Results\\pima-indians-diabetes-test.json", 6, 1.1, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-teach.json", "Results\\pima-indians-diabetes-teach.json", 6, 1.3, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-test.json", "Results\\pima-indians-diabetes-test.json", 6, 1.3, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-teach.json", "Results\\pima-indians-diabetes-teach.json", 6, 1.6, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-test.json", "Results\\pima-indians-diabetes-test.json", 6, 1.6, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-teach.json", "Results\\pima-indians-diabetes-teach.json", 6, 1.8, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-test.json", "Results\\pima-indians-diabetes-test.json", 6, 1.8, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-teach.json", "Results\\pima-indians-diabetes-teach.json", 6, 2, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-test.json", "Results\\pima-indians-diabetes-test.json", 6, 2, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-teach.json", "Results\\pima-indians-diabetes-teach.json", 6, 2.4, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-test.json", "Results\\pima-indians-diabetes-test.json", 6, 2.4, Category = "Pima Indians Diabetes")]

        [TestCase("Data\\pima-indians-diabetes-teach.json", "Results\\pima-indians-diabetes-teach.json", 10, 1.1, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-test.json", "Results\\pima-indians-diabetes-test.json", 10, 1.1, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-teach.json", "Results\\pima-indians-diabetes-teach.json", 10, 1.3, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-test.json", "Results\\pima-indians-diabetes-test.json", 10, 1.3, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-teach.json", "Results\\pima-indians-diabetes-teach.json", 10, 1.6, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-test.json", "Results\\pima-indians-diabetes-test.json", 10, 1.6, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-teach.json", "Results\\pima-indians-diabetes-teach.json", 10, 1.8, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-test.json", "Results\\pima-indians-diabetes-test.json", 10, 1.8, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-teach.json", "Results\\pima-indians-diabetes-teach.json", 10, 2, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-test.json", "Results\\pima-indians-diabetes-test.json", 10, 2, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-teach.json", "Results\\pima-indians-diabetes-teach.json", 10, 2.4, Category = "Pima Indians Diabetes")]
        [TestCase("Data\\pima-indians-diabetes-test.json", "Results\\pima-indians-diabetes-test.json", 10, 2.4, Category = "Pima Indians Diabetes")]


        #endregion
        public void Process(string dataFile, string outputFile, int clustersCount, double fuzziness)
        {
            var data = JsonConvert.DeserializeObject<double[,]>(File.ReadAllText(dataFile));


            var cfg = new Config()
            {
                ClustersCount = clustersCount,
                Data = new DenseMatrix(DenseColumnMajorMatrixStorage<double>.OfArray(data)),
                DataDimensions = data.GetUpperBound(1),
                Fuzziness = fuzziness
            };

            Result result;

            if (!File.Exists(outputFile))
            {
                result = new Result()
                {
                    DataColumnsCount = cfg.Data.ColumnCount,
                    DataRowsCount = cfg.Data.RowCount,
                    DataSetName = Path.GetFileNameWithoutExtension(dataFile),
                    Results = new List<TSResult>()
                };
            }
            else
            {
                result = JsonConvert.DeserializeObject<Result>(File.ReadAllText(outputFile));
            }

            var tsResult = new TSResult()
            {
                Clusters = clustersCount,
                Fuzziness = fuzziness,
                Values = new List<double>()
            };


            for (var i = 0; i < 10; ++i)
            {
                var fcmts = new FCMTS(cfg);
                fcmts.RunFCM();
                var v = fcmts.RunTS(cfg.Data.Column(0));
                tsResult.Values.Add(v);
            }

            tsResult.Min = tsResult.Values.Min();
            tsResult.Max = tsResult.Values.Max();
            tsResult.Avg = tsResult.Values.Average();

            result.Results.Add(tsResult);

            var resultJson = JsonConvert.SerializeObject(result, Formatting.Indented);

            if (!Directory.Exists(Path.GetDirectoryName(outputFile)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(outputFile));
            }

            File.WriteAllText(outputFile, resultJson);
        }

        [TestCase("Results", "Plot", 100000)]
        public void Plot(string srcDir, string dstDir, double ignoreValuesGreaterThan = double.PositiveInfinity)
        {
            var sourceDir = new DirectoryInfo(srcDir);
            var destDir = new DirectoryInfo(dstDir);
            var plot = "set grid\nset term png\nset term png size 400,300\nset ticslevel 0\nset dgrid3d 20,20\nset hidden3d\n";
            plot += "set xlabel 'f'\nset ylabel 'c'\nset zlabel 'q'\nset key off\n\n\n";

            if (!destDir.Exists)
            {
                destDir.Create();
            }

            var files = sourceDir.GetFiles("*.json");

            foreach (var file in files)
            {
                var dataFileName = Path.Combine(destDir.FullName,
                    string.Format("{0}.dat", Path.GetFileNameWithoutExtension(file.Name)));

                var result = JsonConvert.DeserializeObject<Result>(File.ReadAllText(file.FullName));

                var vals = result.Results
                    .Where(t => !t.Values.Any(v => v > ignoreValuesGreaterThan));

                if (!vals.Any())
                {
                    continue;
                }

                var dataStr =
                    vals.Aggregate("#Fuzziness\tClusters\tQuality\n",
                    (current, r) =>
                        current +
                        string.Format(CultureInfo.InvariantCulture, "{0}\t{1}\t{2}\n", r.Fuzziness, r.Clusters, r.Avg));


                plot += string.Format("set out '{0}.png'\n", Path.GetFileNameWithoutExtension(file.Name));
                plot += string.Format("set title '{0}'\n", Path.GetFileNameWithoutExtension(file.Name));
                plot += string.Format("splot '{0}' u 1:2:3 w l\n\n", Path.GetFileName(dataFileName));





                File.WriteAllText(dataFileName, dataStr);
            }

            File.WriteAllText(Path.Combine(destDir.FullName, "plot.gpl"), plot);

        }

        [TestCase("Results", "data.txt")]
        public void FormatData(string srcDir, string outputFile)
        {
            var sourceDir = new DirectoryInfo(srcDir);
            var files = sourceDir.GetFiles("*-teach.json");

            var data = string.Empty;

            foreach (var file in files)
            {
                var teach = JsonConvert.DeserializeObject<Result>(File.ReadAllText(file.FullName));
                var test =
                    JsonConvert.DeserializeObject<Result>(
                        File.ReadAllText(Path.Combine(file.DirectoryName, file.Name.Replace("teach", "test"))));

                data +=
                    string.Format(
                        "|||||||||||\n|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|\n|{0}|\n||||Teach|||Test|||\n|c|f|min|avg|max|min|avg|max|\n",
                        Path.GetFileNameWithoutExtension(file.Name).Replace("-teach", "").Take(7).Aggregate("", (s, c) => s + c));

                foreach (var teachResult in teach.Results)
                {
                    var testResult =
                        test.Results.FirstOrDefault(
                            r => r.Clusters == teachResult.Clusters && Math.Abs(r.Fuzziness - teachResult.Fuzziness) < 0.01d);
                    if (testResult == null)
                        continue;

                    data += string.Format("|{0}|{1}|", teachResult.Clusters, teachResult.Fuzziness);

                    var maxValue = 100000d;

                    data += teachResult.Min < maxValue ? teachResult.Min.ToString("0.####", CultureInfo.InvariantCulture) : teachResult.Min.ToString("E2", CultureInfo.InvariantCulture);
                    data += "|";
                    data += teachResult.Avg < maxValue ? teachResult.Avg.ToString("0.####", CultureInfo.InvariantCulture) : teachResult.Avg.ToString("E2", CultureInfo.InvariantCulture);
                    data += "|";
                    data += teachResult.Max < maxValue ? teachResult.Max.ToString("0.####", CultureInfo.InvariantCulture) : teachResult.Max.ToString("E2", CultureInfo.InvariantCulture);
                    data += "|";
                    data += testResult.Min < maxValue ? testResult.Min.ToString("0.####", CultureInfo.InvariantCulture) : testResult.Min.ToString("E2", CultureInfo.InvariantCulture);
                    data += "|";
                    data += testResult.Avg < maxValue ? testResult.Avg.ToString("0.####", CultureInfo.InvariantCulture) : testResult.Avg.ToString("E2", CultureInfo.InvariantCulture);
                    data += "|";
                    data += testResult.Max < maxValue ? testResult.Max.ToString("0.####", CultureInfo.InvariantCulture) : testResult.Max.ToString("E2", CultureInfo.InvariantCulture);
                    data += "|\n";




                }



                //data += teach.Results

                //        .Select(r => new { m = r.Fuzziness, c = r.Clusters, min = r.Min, avg = r.Avg, max = r.Max })
                //        .OrderBy(t => t.c)
                //        .ThenBy(t => t.m)
                //        .Aggregate("", (s, t) =>
                //            s + string.Format(CultureInfo.InvariantCulture,
                //            "|{0}|{1}|{2:R}|{3:R}|{4:R}|\n",
                //            t.c, t.m, t.min, t.avg, t.max));

                //data += string.Format("Data Set: {0}\n\nc\t\tf\t\t\t\tmin\t\t\t\tavg\t\t\t\tmax\n", Path.GetFileNameWithoutExtension(file.Name));

                //data += result.Results

                //    .Select(r => new { m = r.Fuzziness, c = r.Clusters, min = r.Min, avg = r.Avg, max = r.Max })
                //    .OrderBy(t => t.c)
                //    .ThenBy(t => t.m)
                //    .Aggregate("", (s, t) => 
                //        s + string.Format(CultureInfo.InvariantCulture, 
                //        "{0}\t\t{1}\t\t\t\t{2:R}\t\t\t\t{3:R}\t\t\t\t{4:R}\n", 
                //        t.c, t.m, t.min, t.avg, t.max));

                data += "\n\n";
            }

            File.WriteAllText(outputFile, data);


        }

    }
}
