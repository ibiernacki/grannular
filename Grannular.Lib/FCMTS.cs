﻿using System;
using System.Collections.Generic;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using MathNet.Numerics.LinearAlgebra.Storage;

namespace Grannular.Lib
{
    public class FCMTS
    {
        private static readonly Random _r = new Random();

        public readonly Config Config;
        public readonly int N;
        public readonly int Dimensions;

        public DenseMatrix U { get; private set; }
        public DenseMatrix Clusters { get; private set; }


        private DenseVector _y;
        private DenseMatrix _g;

        public FCMTS(Config config)
        {
            Config = config;

            Dimensions = Config.Data.ColumnCount;
            N = Config.Data.RowCount;

            U = new DenseMatrix(N, Config.ClustersCount);
            Clusters = new DenseMatrix(Config.ClustersCount, Dimensions);
        }

        private void CalculateCenters()
        {
            var tmp = new double[N, Config.ClustersCount];

            for (var i = 0; i < N; i++)
            {
                for (var j = 0; j < Config.ClustersCount; j++)
                {
                    tmp[i, j] = Math.Pow(U[i, j], Config.Fuzziness);
                }
            }

            for (var j = 0; j < Config.ClustersCount; j++)
            {
                for (var k = 0; k < Dimensions; k++)
                {
                    var numerator = 0.0;
                    var denominator = 0.0;
                    for (var i = 0; i < N; i++)
                    {
                        numerator += tmp[i, j] * Config.Data[i, k];
                        denominator += tmp[i, j];
                    }
                    Clusters[j, k] = numerator / denominator;
                }
            }
        }


        private void RandomizeU()
        {
            for (var i = 0; i < N; ++i)
            {
                for (var j = 0; j < Config.ClustersCount; ++j)
                {
                    U[i, j] = _r.NextDouble();
                }
            }
        }
        private void NormalizeU()
        {

            for (var i = 0; i < U.RowCount; ++i)
            {
                var sum = U.Row(i).Sum();
                for (var j = 0; j < Config.ClustersCount; ++j)
                {
                    U[i, j] /= sum;
                }
            }
        }

        public void RunFCM()
        {
            RandomizeU();
            NormalizeU();
            //CalculateCenters();

            var m = 1d;
            do
            {
                CalculateCenters();
                m = CalculateU();
            } while (m > Config.Epsilon);
        }

        private double CalculateU()
        {
            var max = 0.0;
            for (var j = 0; j < Config.ClustersCount; ++j)
                for (var i = 0; i < N; ++i)
                {
                    var n = CalculateUVal(i, j);
                    var diff = n - U[i, j];
                    diff = Math.Abs(diff);
                    U[i, j] = n;
                    if (max < diff)
                        max = diff;
                }
            return max;
        }

        private double CalculateUVal(int i, int j)
        {
            var sum = 0d;
            var p = 2d / (Config.Fuzziness - 1d);
            for (var k = 0; k < Config.ClustersCount; k++)
            {
                var t = CalculateVecNorm(i, j) / CalculateVecNorm(i, k);
                t = Math.Pow(t, p);
                sum += t;
            }
            return 1d / sum;
        }

        private double CalculateVecNorm(int i, int j)
        {
            var sum = 0d;
            for (var k = 0; k < Dimensions; ++k)
            {
                sum += Math.Pow(Config.Data[i, k] - Clusters[j, k], 2);
            }
            return Math.Sqrt(sum);
        }


        public double RunTS(Vector<double> yVals)
        {
            _y = DenseVector.OfVector(yVals);
            CalculateG();
            return CalculateQuality();
        }

        private void CalculateG()
        {
            _g = new DenseMatrix(N, Config.ClustersCount * (Dimensions + 1));


            for (var i = 0; i < N; i++)
            {
                for (var j = 0; j < Config.ClustersCount; j++)
                {
                    _g[i, j * (Dimensions + 1)] = U[i, j] * 1d;
                    for (var k = 0; k < Dimensions; k++)
                    {
                        _g[i, j * (Dimensions + 1) - 1 + (k + 2)] = U[i, j] * Config.Data[i, k];
                    }
                }
            }
        }

        private double CalculateQuality()
        {

            //a = (G^T * G)^-1*G*y
            var gT = _g.Transpose();
            var tmp = gT.Multiply(_g).Inverse();
            var a = tmp.Multiply(gT).Multiply(_y);


            Vector<double> y_est = new DenseVector(N);
            for (var i = 0; i < Math.Min(N, a.Count); ++i)
                y_est[i] = a[i];

            var y_estVals = _g.LeftMultiply(y_est);

            for (var i = 0; i < Math.Min(N, y_estVals.Count); ++i)
                y_est[i] = y_estVals[i];

            var result = 0d;

            for (var i = 0; i < N; i++)
            {
                result += (_y[i] - y_est[i]) * (_y[i] - y_est[i]);
            }
            var endResult = Math.Sqrt(result / N);
            return endResult;


        }
    }
}
