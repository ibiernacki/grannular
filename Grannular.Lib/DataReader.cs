﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra.Double;

namespace Grannular.Lib
{
    public class DataReader
    {
        private const string DataRegex = @"((?<=\s*)[0-9.,e-]*(?=\s|$))";

        private IList<double> ParseLine(string line)
        {
            return
                Regex.Matches(line, DataRegex, RegexOptions.IgnorePatternWhitespace)
                    .Cast<Match>()
                    .Where(m => !string.IsNullOrWhiteSpace(m.Value))
                    .Select(m => double.Parse(m.Value, CultureInfo.InvariantCulture))
                    .ToList();
        }

        public Config Read(FileInfo file)
        {
            var config = new Config();
            var txt = File.ReadAllText(file.FullName).Replace("\r", "").TrimEnd(' ', '\n');
            var lines = txt.Split('\n');
            config.DataDimensions = ParseLine(lines[0]).Count;

            config.Data = new DenseMatrix(lines.Length, config.DataDimensions);

            for (var i = 0; i < lines.Length; ++i)
            {
                var values = ParseLine(lines[i]);
                for (var j = 0; j < config.DataDimensions; ++j)
                {
                    config.Data[i, j] = values[j];
                }
            }
            return config;
        }


    }
}
