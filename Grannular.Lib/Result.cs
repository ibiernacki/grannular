﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grannular.Lib
{
    public class Result
    {
        public string DataSetName { get; set; }
        public int DataColumnsCount { get; set; }
        public int DataRowsCount { get; set; }
        public IList<TSResult> Results { get; set; }
    }
}
