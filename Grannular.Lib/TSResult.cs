﻿using System.Collections.Generic;

namespace Grannular.Lib
{
    public class TSResult
    {
        public int Clusters { get; set; }
        public double Fuzziness { get; set; }
        public IList<double> Values { get; set; }
        public double Avg { get; set; }
        public double Min { get; set; }
        public double Max { get; set; }
    }
}