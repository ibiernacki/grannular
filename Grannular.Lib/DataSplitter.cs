﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra.Double;

namespace Grannular.Lib
{
    public static class DataSplitter
    {
        private static readonly Random _r = new Random();

        public static void Split(DenseMatrix data, out DenseMatrix teachData, out DenseMatrix testData)
        {
            var srcData = data.Clone();

            var teachDataCount = data.RowCount / 10 * 7;
            var testDataCount = data.RowCount - teachDataCount;

            teachData = new DenseMatrix(teachDataCount, data.ColumnCount);
            testData = new DenseMatrix(testDataCount, data.ColumnCount);

            for (var i = 0; i < teachDataCount; ++i)
            {
                var rowIndex = _r.Next(0, srcData.RowCount);
                teachData.SetRow(i, srcData.Row(rowIndex));
                srcData = srcData.RemoveRow(rowIndex);
            }

            for (var i = 0; i < testDataCount - 1; ++i)
            {
                var rowIndex = _r.Next(0, srcData.RowCount);
                testData.SetRow(i, srcData.Row(rowIndex));

                srcData = srcData.RemoveRow(rowIndex);
            }

            testData.SetRow(testDataCount - 1, srcData.Row(0));
        }
    }
}
