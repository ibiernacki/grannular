﻿using MathNet.Numerics.LinearAlgebra.Double;

namespace Grannular.Lib
{
    public class Config
    {

        public Config()
        {
            Epsilon = 1e-6;
        }

        public double Epsilon { get; set; }
        public double Fuzziness { get; set; }
        public int DataDimensions { get; set; }
        public int ClustersCount { get; set; }
        public DenseMatrix Data { get; set; }
    }
}
